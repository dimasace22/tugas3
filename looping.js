console.log("=====No. 1 Looping While=====");
console.log(


);
console.log("LOOPING PERTAMA");
console.log(

);
var flag = 2;
while(flag <= 20) { // Loop akan terus berjalan selama nilai flag masih dibawah atau sama dengan 20
  console.log(flag +' '+ " - " +'I love coding'); // Menampilkan nilai flag pada iterasi tertentu
  flag+=2; // Mengubah nilai flag dengan menambahkan 2
}
console.log(

);
console.log("LOOPING KEDUA");
console.log(

);
var deret = 22;
while(deret > 2) { // Loop akan terus berjalan selama nilai deret masih di atas 0
  deret-=2; // Mengubah nilai deret dengan mengurangi 2
  console.log(deret +' '+ " - " +'I will become a mobile developer')
}
console.log(


);
console.log("No. 2 Looping menggunakan for");
console.log(


);
for(var ganjil = 1; ganjil <= 20; ganjil++) {
      if (ganjil % 2 != 0 && ganjil % 3 !=0)  {
        console.log(ganjil + " - "+ "Santai");
      }
      if (ganjil % 2 == 0 ) {
        console.log(ganjil + " - Berkualitas");
      } 
      if (ganjil % 3 == 0 && ganjil % 2 !== 0) {
        console.log(ganjil + " - I Love Coding");
      }
}
console.log(


);
console.log("=====No. 3 Membuat Persegi Panjang=====");
console.log(


);
console.log("########");
console.log("########");
console.log("########");
console.log("########");
console.log(


);
console.log("=====No. 4 Membuat Tangga ======");
console.log(


);
console.log("#");
console.log("##");
console.log("###");
console.log("####");
console.log("#####");
console.log("######");
console.log("#######");
console.log(


);
console.log("=====No. 5 Membuat Papan Catur=====");
console.log(


);
console.log(" # # # #");
console.log("# # # #");
console.log(" # # # #");
console.log("# # # #");
console.log(" # # # #");
console.log("# # # #");
console.log(" # # # #");
console.log("# # # #");